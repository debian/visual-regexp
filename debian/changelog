visual-regexp (3.1-7) unstable; urgency=medium

  * QA upload.
  * debian/copyright:
      - Added a new block for
        debian/upstream/fr.free.riesterer.laurent.visual_regexp.metainfo.xml.
      - Added text of MIT license.
  * debian/icons/*: created to provide icon in desktop menu.
  * debian/install: added lines to install icons and metainfo files.
  * debian/source/include-binaries: added to allow
    debian/icons/128x128/visual-regexp.png file.
  * debian/upstream/fr.free.riesterer.laurent.visual_regexp.metainfo.xml:
    created to provide metainfo file.

 -- Braulio Henrique Marques Souto <braulio@disroot.org>  Thu, 10 Nov 2022 19:10:27 -0300

visual-regexp (3.1-6) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * Converted and updated the manpage to txt2man system. Consequently:
      - Created:
          ~ debian/manpage/ directory.
          ~ debian/manpages file.
      - Removed:
          ~ 'Build-Depends-Indep: docbook-to-man' in debian/control.
          ~ debian/visual-regexp.1.
          ~ debian/visual-regexp.manpages.
          ~ debian/visual-regexp.sgml.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Added 'perl-doc' to 'Suggests' field.
      - Added the Vcs-* fields.
      - Bumped Standards-Version to 4.6.1.
      - Changed short description.
      - Changed 'tclvfs' to 'tcl-vfs' in Depends field.
      - Improved the long description.
  * debian/copyright:
      - Added a new block for debian/manpage/create-man.sh.
      - Added Upstream-Contact field.
      - Changed Upstream-Name.
      - Updated packaging copyright years and data.
      - Use https protocol in Format field.
  * debian/dirs: removed in favor of dh_auto_install.
  * debian/install: created to install debian/visual-regexp.desktop.
  * debian/manpage/*: created files to produce the manpage.
  * debian/manpages: created to install the manpage.
  * debian/menu: removed in favor of debian/visual-regexp.desktop.
  * debian/patches:
      - Added a numerical prefix to all patches.
      - Refreshed all patches.
  * debian/rules:
      - Added target override_dh_installchangelogs to install
        debian/upstream/changelog.
      - Creating the main directory in override_dh_auto_install.
      - Removed unnecessary lines.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/source/lintian-overrides: created to override a message about
    the manpage and .desktop file, because it was already sent to upstream.
    This override was requested by lintian.
  * debian/upstream/changelog: added as a changelog from upstream homepage.
  * debian/upstream/metadata: created.
  * debian/visual-regexp.desktop: created to provide the .desktop file.
    (Closes: #766987)
  * debian/watch:
      - Bumped to version 4.
      - Updated the search rule.

 -- Braulio Henrique Marques Souto <braulio@disroot.org>  Mon, 17 Oct 2022 22:56:22 -0300

visual-regexp (3.1-5.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 14:52:33 +0100

visual-regexp (3.1-5) unstable; urgency=medium

  * QA upload.
  * Switch to the default Tcl/Tk version since Tcl/Tk 8.5 is to be removed
    from Debian.
  * Bump the standards version to 4.1.3.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 17 Mar 2018 08:30:55 +0300

visual-regexp (3.1-4) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #790479)
  * DH level to 9.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Revised and updated all information.
  * debian/control
      - Bumped Standards-Version to 3.9.6.
  * debian/rules: improved organization.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Thu, 24 Sep 2015 20:08:48 -0300

visual-regexp (3.1-3) unstable; urgency=low

  * Added patch to fix the option make regexp (Closes: #594972)

 -- Wences Arana <aranax@debian.org.gt>  Tue, 21 Jun 2011 18:49:50 -0600

visual-regexp (3.1-2) unstable; urgency=low

  * Patch to fix the interpreter location and version  (Closes: #592743)

 -- Wences René Arana Fuentes <aranax@debian.org.gt>  Thu, 12 Aug 2010 22:05:25 -0600

visual-regexp (3.1-1) unstable; urgency=low

  * New upstream release
  * Switch to dpkg-source 3.0 (quilt) format
  * Switch to debhelper 7
  * New maintainer
    - Thanks to Corrin Lakeland for his previous work (closes: Bug#519351)
  * Patch to fix the interpreter to #!/usr/bin/wish and
    set the right version.
  * Added get-orig-sources in debian/rules
  * Added debian/Readme.Source
  * Added dependence tclvfs

 -- Wences Rene Arana Fuentes <aranaf51@gmail.com>  Thu, 10 Jun 2010 22:43:15 -0600

visual-regexp (3.0-2) unstable; urgency=low

  * QA upload.
    + Set maintainer to Debian QA Group <packages@qa.debian.org>.
  * Don't require wish8.3. (Closes: #521366).
    + Add optional depends on tk8.4, tk8.5.
  * Update watch file. (Closes: #449995, #529145).
  * Add Homepage.
  * Add ${misc:Depends} for debhelper package.
  * Quote strings in menu file.
  * Add appropriate copyright holder in debian/copyright.
  * Move debhelper to build-depends to satisfy clean.
  * Bump debhelper build-dep and compat to 5.
    + Remove DH_COMPAT from rules even though compat existed.
  * Bump Standards Version to 3.8.3.
    + Menu policy transition.

 -- Barry deFreese <bdefreese@debian.org>  Sun, 08 Nov 2009 22:39:20 -0500

visual-regexp (3.0-1) unstable; urgency=low

  * New upstream
    Upstream already has 3.1 available, but not for the complete package.
  * Updated a little of the documentation to reflect that this is version 3.0

 -- Corrin Lakeland <lakeland@debian.org>  Wed,  5 Apr 2006 15:52:14 +1200

visual-regexp (2.2r1-1) unstable; urgency=low

  * Bumped version to allow upload of .orig file
  * Change references in the docs to the new name (Closes: 190858)

 -- Corrin Lakeland <lakeland@debian.org>  Sun,  1 Jun 2003 17:29:51 +1200

visual-regexp (2.2-2) unstable; urgency=low

  * Fixed syntax for build depends on docbook-to-man (Closes: 142102)

 -- Corrin Lakeland <lakeland@debian.org>  Sat, 13 Apr 2002 10:19:52 +1200

visual-regexp (2.2-1) unstable; urgency=low

  * Updated to new version (Closes: 136095)

 -- Corrin Lakeland <lakeland@debian.org>  Fri,  1 Mar 2002 18:23:42 +1300

visual-regexp (2.1-5) unstable; urgency=low

  * Added binary-depends-indep on docbook-to-man (Closes: 134902)

 -- Corrin Lakeland <lakeland@debian.org>  Mon, 25 Feb 2002 20:20:16 +1300

visual-regexp (2.1-4) unstable; urgency=low

  * Fixed the spelling of interatively (Closes: 125459)

 -- Corrin Lakeland <lakeland@debian.org>  Fri, 22 Feb 2002 09:09:55 +1300

visual-regexp (2.1-3) unstable; urgency=low

  * Corrected the depends field: tcl != tk :-) (Closes: 93741)

 -- Corrin Lakeland <lakeland@debian.org>  Mon, 16 Apr 2001 10:27:49 +1200

visual-regexp (2.1-2) unstable; urgency=low

  * Fixed the arch field

 -- Corrin Lakeland <lakeland@debian.org>  Wed, 11 Apr 2001 18:39:19 +1200

visual-regexp (2.1-1) unstable; urgency=low

  * Initial Release.
  * 'makefile' written, copies script to /usr/bin

 -- Corrin Lakeland <lakeland@debian.org>  Wed, 11 Apr 2001 18:39:14 +1200

Local variables:
mode: debian-changelog
End:
